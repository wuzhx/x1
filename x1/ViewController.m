//
//  ViewController.m
//  x1
//
//  Created by wuzhx on 16/6/20.
//  Copyright © 2016年 onestep. All rights reserved.
//

#import "ViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setForegroundColor:[UIColor clearColor]];
//    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD setFont:[UIFont systemFontOfSize:20]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    imageView.image = [UIImage imageNamed:@"1.png"];
    [self.view addSubview:imageView];
//    [imageView sizeToFit];
    CGSize size = imageView.image.size;
    imageView.bounds = CGRectMake(0, 0, 142*0.347, 142*0.347);
    imageView.center = self.view.center;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnShowAction:(id)sender
{
    [SVProgressHUD showWithStatus:@"loading..."];
}

- (IBAction)btnShowWithImageAction:(id)sender
{
    [SVProgressHUD showSuccessWithStatus:@"loading..." maskType:SVProgressHUDMaskTypeBlack];
//    [SVProgressHUD showImage:[UIImage imageNamed:@"lbs.gif"] status:@"loading..."];
}

- (IBAction)btnShowWithSuccessAction:(id)sender
{
    [SVProgressHUD showSuccessWithStatus:@"success"];
}

- (IBAction)btnShowWithErrorAction:(id)sender
{
    [SVProgressHUD showErrorWithStatus:@"error"];
}

- (IBAction)btnHideAction:(id)sender
{
    [SVProgressHUD popActivity];
}

@end
