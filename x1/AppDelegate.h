//
//  AppDelegate.h
//  x1
//
//  Created by wuzhx on 16/6/20.
//  Copyright © 2016年 onestep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

