//
//  main.m
//  x2
//
//  Created by wuzhx on 16/6/24.
//  Copyright © 2016年 onestep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
