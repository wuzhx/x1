//
//  ViewController.m
//  x2
//
//  Created by wuzhx on 16/6/24.
//  Copyright © 2016年 onestep. All rights reserved.
//

#import "ViewController.h"
//#import <CAProgressHUD/SVProgressHUD.h>
#import <xtest/CAPlatform.h>
#import <QuartzCore/QuartzCore.h>

#import <sys/sysctl.h>
#import <string>
using namespace std;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *imagesArray = [NSArray arrayWithObjects:
                            [UIImage imageNamed:@"ca_loading_1.png"],
                            [UIImage imageNamed:@"ca_loading_2.png"],
                            [UIImage imageNamed:@"ca_loading_3.png"],
                            [UIImage imageNamed:@"ca_loading_4.png"],
                            [UIImage imageNamed:@"ca_loading_5.png"],
                            [UIImage imageNamed:@"ca_loading_6.png"],
                            [UIImage imageNamed:@"ca_loading_7.png"],
                            [UIImage imageNamed:@"ca_loading_8.png"],
                            [UIImage imageNamed:@"ca_loading_9.png"],
                            [UIImage imageNamed:@"ca_loading_10.png"],
                            [UIImage imageNamed:@"ca_loading_11.png"],
                            [UIImage imageNamed:@"ca_loading_12.png"],
                            [UIImage imageNamed:@"ca_loading_13.png"],
                            [UIImage imageNamed:@"ca_loading_14.png"],
                            [UIImage imageNamed:@"ca_loading_15.png"],
                            [UIImage imageNamed:@"ca_loading_16.png"],
                            [UIImage imageNamed:@"ca_loading_17.png"],nil];
//    [SVProgressHUD setLoaingAnimationImages:imagesArray imageSize:CGSizeMake(100, 100)];
//    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
    
    self.view.backgroundColor = [UIColor grayColor];
    [self drawCALogo];
}

- (void)drawCALogo
{
    //// Color Declarations
    UIColor* color1 = [UIColor colorWithRed: 0.521 green: 0.521 blue: 0.521 alpha: 1];
    UIColor* color2 = [UIColor colorWithRed: 0.521 green: 0.521 blue: 0.521 alpha: 1];
    
    //// Page-1
    {
        //// Bezier Drawing
        UIBezierPath* bezierPath = UIBezierPath.bezierPath;
        [bezierPath moveToPoint: CGPointMake(141.33, 29.21)];
        [bezierPath addCurveToPoint: CGPointMake(144.56, 55.15) controlPoint1: CGPointMake(144.3, 37.64) controlPoint2: CGPointMake(144.56, 55.15)];
        [bezierPath addCurveToPoint: CGPointMake(181.59, 57.87) controlPoint1: CGPointMake(144.56, 55.15) controlPoint2: CGPointMake(169.56, 56.01)];
        [bezierPath addCurveToPoint: CGPointMake(221.21, 66.65) controlPoint1: CGPointMake(194.79, 59.91) controlPoint2: CGPointMake(221.21, 66.65)];
        [bezierPath addCurveToPoint: CGPointMake(210.88, 76.71) controlPoint1: CGPointMake(221.21, 66.65) controlPoint2: CGPointMake(213.91, 73.22)];
        [bezierPath addCurveToPoint: CGPointMake(205.85, 83.35) controlPoint1: CGPointMake(209.63, 78.14) controlPoint2: CGPointMake(208.83, 79.64)];
        [bezierPath addCurveToPoint: CGPointMake(202.61, 92.5) controlPoint1: CGPointMake(202.87, 87.06) controlPoint2: CGPointMake(202.61, 92.5)];
        [bezierPath addCurveToPoint: CGPointMake(208.83, 142.43) controlPoint1: CGPointMake(202.61, 92.5) controlPoint2: CGPointMake(210.88, 114.82)];
        [bezierPath addCurveToPoint: CGPointMake(195.91, 181.6) controlPoint1: CGPointMake(207.91, 154.81) controlPoint2: CGPointMake(207.25, 168.59)];
        [bezierPath addCurveToPoint: CGPointMake(183.11, 194.19) controlPoint1: CGPointMake(192.02, 186.06) controlPoint2: CGPointMake(187.73, 190.71)];
        [bezierPath addCurveToPoint: CGPointMake(151.21, 208.34) controlPoint1: CGPointMake(174.27, 200.86) controlPoint2: CGPointMake(151.21, 208.34)];
        [bezierPath addCurveToPoint: CGPointMake(86.96, 206.64) controlPoint1: CGPointMake(151.21, 208.34) controlPoint2: CGPointMake(104.4, 213.95)];
        [bezierPath addCurveToPoint: CGPointMake(56.16, 190.79) controlPoint1: CGPointMake(79.74, 203.61) controlPoint2: CGPointMake(71.35, 203.27)];
        [bezierPath addCurveToPoint: CGPointMake(39.49, 166.48) controlPoint1: CGPointMake(40.97, 178.32) controlPoint2: CGPointMake(39.49, 166.48)];
        [bezierPath addCurveToPoint: CGPointMake(57.97, 167.54) controlPoint1: CGPointMake(39.49, 166.48) controlPoint2: CGPointMake(49.61, 169.85)];
        [bezierPath addCurveToPoint: CGPointMake(68.43, 159.49) controlPoint1: CGPointMake(66.32, 165.23) controlPoint2: CGPointMake(68.43, 159.49)];
        [bezierPath addCurveToPoint: CGPointMake(50.09, 148.16) controlPoint1: CGPointMake(68.43, 159.49) controlPoint2: CGPointMake(55.5, 152.04)];
        [bezierPath addCurveToPoint: CGPointMake(40.73, 132.43) controlPoint1: CGPointMake(44.68, 144.29) controlPoint2: CGPointMake(40.73, 132.43)];
        [bezierPath addCurveToPoint: CGPointMake(46.26, 103.48) controlPoint1: CGPointMake(40.73, 132.43) controlPoint2: CGPointMake(39.49, 115.04)];
        [bezierPath addCurveToPoint: CGPointMake(57.97, 85.49) controlPoint1: CGPointMake(53.04, 91.93) controlPoint2: CGPointMake(50.56, 93.37)];
        [bezierPath addCurveToPoint: CGPointMake(83.05, 65.01) controlPoint1: CGPointMake(68.55, 74.23) controlPoint2: CGPointMake(83.05, 65.01)];
        [bezierPath addCurveToPoint: CGPointMake(83.05, 51.57) controlPoint1: CGPointMake(83.05, 65.01) controlPoint2: CGPointMake(84.1, 54.64)];
        [bezierPath addCurveToPoint: CGPointMake(75.88, 47.02) controlPoint1: CGPointMake(81.85, 48.05) controlPoint2: CGPointMake(75.88, 47.02)];
        [bezierPath addCurveToPoint: CGPointMake(68.43, 47.02) controlPoint1: CGPointMake(75.88, 47.02) controlPoint2: CGPointMake(70.97, 46.11)];
        [bezierPath addCurveToPoint: CGPointMake(52.55, 56.55) controlPoint1: CGPointMake(64.53, 48.43) controlPoint2: CGPointMake(57.48, 54.11)];
        [bezierPath addCurveToPoint: CGPointMake(44.68, 58.23) controlPoint1: CGPointMake(47.55, 59.02) controlPoint2: CGPointMake(44.68, 58.23)];
        [bezierPath addCurveToPoint: CGPointMake(34.85, 57.87) controlPoint1: CGPointMake(44.68, 58.23) controlPoint2: CGPointMake(37.03, 59.44)];
        [bezierPath addCurveToPoint: CGPointMake(27.21, 51.57) controlPoint1: CGPointMake(32.78, 56.38) controlPoint2: CGPointMake(29.06, 54.53)];
        [bezierPath addCurveToPoint: CGPointMake(27.21, 45.64) controlPoint1: CGPointMake(25.81, 49.33) controlPoint2: CGPointMake(27.21, 45.64)];
        [bezierPath addCurveToPoint: CGPointMake(32.95, 39.96) controlPoint1: CGPointMake(27.21, 45.64) controlPoint2: CGPointMake(28.99, 42.59)];
        [bezierPath addCurveToPoint: CGPointMake(42.84, 35.99) controlPoint1: CGPointMake(35.77, 38.08) controlPoint2: CGPointMake(40.52, 36.5)];
        [bezierPath addCurveToPoint: CGPointMake(73.61, 34.73) controlPoint1: CGPointMake(53.49, 33.65) controlPoint2: CGPointMake(73.61, 34.73)];
        [bezierPath addCurveToPoint: CGPointMake(91.21, 42.46) controlPoint1: CGPointMake(73.61, 34.73) controlPoint2: CGPointMake(86.64, 38.67)];
        [bezierPath addCurveToPoint: CGPointMake(100.37, 59.65) controlPoint1: CGPointMake(95.93, 46.38) controlPoint2: CGPointMake(100.37, 59.65)];
        [bezierPath addCurveToPoint: CGPointMake(111.13, 56.55) controlPoint1: CGPointMake(100.37, 59.65) controlPoint2: CGPointMake(107.1, 57.15)];
        [bezierPath addCurveToPoint: CGPointMake(121.23, 56.55) controlPoint1: CGPointMake(114.05, 56.11) controlPoint2: CGPointMake(121.23, 56.55)];
        [bezierPath addCurveToPoint: CGPointMake(123.9, 45.64) controlPoint1: CGPointMake(121.23, 56.55) controlPoint2: CGPointMake(123.01, 52.2)];
        [bezierPath addCurveToPoint: CGPointMake(123.9, 29.21) controlPoint1: CGPointMake(124.64, 40.16) controlPoint2: CGPointMake(124.6, 32.68)];
        [bezierPath addCurveToPoint: CGPointMake(116.69, 18.09) controlPoint1: CGPointMake(122.71, 23.3) controlPoint2: CGPointMake(116.69, 18.09)];
        [bezierPath addCurveToPoint: CGPointMake(104.74, 16.64) controlPoint1: CGPointMake(116.69, 18.09) controlPoint2: CGPointMake(109.34, 15.89)];
        [bezierPath addCurveToPoint: CGPointMake(86.96, 23.04) controlPoint1: CGPointMake(100.75, 17.3) controlPoint2: CGPointMake(92.4, 21.51)];
        [bezierPath addCurveToPoint: CGPointMake(73.61, 25.12) controlPoint1: CGPointMake(79.12, 25.25) controlPoint2: CGPointMake(73.61, 25.12)];
        [bezierPath addCurveToPoint: CGPointMake(62.04, 21.96) controlPoint1: CGPointMake(73.61, 25.12) controlPoint2: CGPointMake(65.07, 24.77)];
        [bezierPath addCurveToPoint: CGPointMake(55.02, 14.43) controlPoint1: CGPointMake(60.55, 20.58) controlPoint2: CGPointMake(56.17, 17.51)];
        [bezierPath addCurveToPoint: CGPointMake(55.02, 8.65) controlPoint1: CGPointMake(54.02, 11.75) controlPoint2: CGPointMake(55.02, 8.65)];
        [bezierPath addCurveToPoint: CGPointMake(60.47, 4.27) controlPoint1: CGPointMake(55.02, 8.65) controlPoint2: CGPointMake(57.15, 6.17)];
        [bezierPath addCurveToPoint: CGPointMake(69.78, 1.47) controlPoint1: CGPointMake(63.47, 2.56) controlPoint2: CGPointMake(67.62, 2.04)];
        [bezierPath addCurveToPoint: CGPointMake(103.25, 1.47) controlPoint1: CGPointMake(73.61, 0.46) controlPoint2: CGPointMake(91.28, -0.67)];
        [bezierPath addCurveToPoint: CGPointMake(130.08, 10.97) controlPoint1: CGPointMake(116.97, 3.93) controlPoint2: CGPointMake(130.08, 10.97)];
        [bezierPath addCurveToPoint: CGPointMake(141.33, 29.21) controlPoint1: CGPointMake(130.08, 10.97) controlPoint2: CGPointMake(139.14, 22.98)];
        [bezierPath closePath];
        bezierPath.miterLimit = 4;
        
        bezierPath.usesEvenOddFillRule = YES;
        
        [color1 setStroke];
        bezierPath.lineWidth = 1;
        [bezierPath stroke];
        
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        layer.path = bezierPath.CGPath;
        layer.position = CGPointMake(100, 300);
        layer.bounds = CGPathGetBoundingBox(layer.path);
        layer.fillColor = [UIColor whiteColor].CGColor;
        [self.view.layer addSublayer:layer];
        
        
        
        UIBezierPath* bezier2Path = UIBezierPath.bezierPath;
        [bezier2Path moveToPoint: CGPointMake(93.43, 170.72)];
        [bezier2Path addCurveToPoint: CGPointMake(83.5, 161.62) controlPoint1: CGPointMake(90.5, 168.71) controlPoint2: CGPointMake(85.46, 164.64)];
        [bezier2Path addCurveToPoint: CGPointMake(83.5, 158.23) controlPoint1: CGPointMake(82.39, 159.91) controlPoint2: CGPointMake(83.5, 158.23)];
        [bezier2Path addCurveToPoint: CGPointMake(107.08, 151.28) controlPoint1: CGPointMake(83.5, 158.23) controlPoint2: CGPointMake(95.21, 156.21)];
        [bezier2Path addCurveToPoint: CGPointMake(127.73, 138.45) controlPoint1: CGPointMake(114.93, 148.02) controlPoint2: CGPointMake(122.7, 142.84)];
        [bezier2Path addCurveToPoint: CGPointMake(149.46, 105.8) controlPoint1: CGPointMake(140.38, 127.42) controlPoint2: CGPointMake(149.46, 105.8)];
        [bezier2Path addCurveToPoint: CGPointMake(160.61, 114.52) controlPoint1: CGPointMake(149.46, 105.8) controlPoint2: CGPointMake(155.74, 104.76)];
        [bezier2Path addCurveToPoint: CGPointMake(163.39, 136.59) controlPoint1: CGPointMake(165.49, 124.29) controlPoint2: CGPointMake(164.17, 129.99)];
        [bezier2Path addCurveToPoint: CGPointMake(151, 160.28) controlPoint1: CGPointMake(161.54, 152.28) controlPoint2: CGPointMake(151, 160.28)];
        [bezier2Path addCurveToPoint: CGPointMake(140.2, 169.32) controlPoint1: CGPointMake(151, 160.28) controlPoint2: CGPointMake(144.54, 167.01)];
        [bezier2Path addCurveToPoint: CGPointMake(123.46, 174.94) controlPoint1: CGPointMake(135.36, 171.9) controlPoint2: CGPointMake(123.46, 174.94)];
        [bezier2Path addLineToPoint: CGPointMake(108.27, 175.2)];
        [bezier2Path addCurveToPoint: CGPointMake(93.43, 170.72) controlPoint1: CGPointMake(108.27, 175.2) controlPoint2: CGPointMake(97.09, 173.23)];
        [bezier2Path closePath];
        bezier2Path.miterLimit = 4;
        
        bezier2Path.usesEvenOddFillRule = YES;
        
        [color2 setStroke];
        bezier2Path.lineWidth = 1;
        [bezier2Path stroke];
        
        CAShapeLayer *layer2 = [[CAShapeLayer alloc] init];
        layer2.path = bezier2Path.CGPath;
        layer2.position = CGPointMake(100, 330);
        layer2.bounds = CGPathGetBoundingBox(layer2.path);
        layer2.fillColor = [UIColor grayColor].CGColor;
        [self.view.layer addSublayer:layer2];
    }
    
}

- (IBAction)btnShowAction:(id)sender
{
//    [SVProgressHUD show];
    
    NSArray *arr = @[@0, @1, @2];
    NSLog(@"arr[3] = %@", arr[3]);
}

- (IBAction)btnHideAction:(id)sender
{
//    [SVProgressHUD dismiss];
    
    NSLog(@"test signal exception");
    NSString * null = nil;
    NSLog(@"print the nil string %s", [null UTF8String]);
    NSLog(@"print the nil string to c string: %s", std::string([null UTF8String]).c_str());
}

- (IBAction)btnOpenSDK:(id)sender
{
    [[CAPlatform sharedInstance] showMain];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
