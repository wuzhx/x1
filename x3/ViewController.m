//
//  ViewController.m
//  x3
//
//  Created by wuzhx on 16/7/21.
//  Copyright © 2016年 onestep. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSLog(@"documentsDir = %@", documentsDir);
    
    [self createNum2Bit];
    [self createNum3Bit];
    [self createNum4Bit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createNum4Bit
{
    NSMutableArray *str1Arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; i++) {
        NSString *str1 = @"";
        for (int j = 0; j < 10; j++) {
            for (int k = 0; k < 10; k++) {
                for (int l = 0; l < 10; l++) {
                    str1 = [str1 stringByAppendingFormat:@"%d%d%d%d\n", i, j, k, l];
                }
            }
        }
        [str1Arr addObject:str1];
    }
    NSString *str = @"";
    for (NSString *str1 in str1Arr) {
        str = [str stringByAppendingString:str1];
    }
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *filePath = [documentsDir stringByAppendingPathComponent:@"num4bit"];
    NSError *err = [[NSError alloc] init];
    [str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&err];
    if (err.code == 0) {
        NSLog(@"保存成功");
    }else {
        NSLog(@"err: 保存失败 - %@", err);
    }
}

- (void)createNum3Bit
{
    NSString *str = @"";
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            for (int k = 0; k < 10; k++) {
                str = [str stringByAppendingFormat:@"%d%d%d\n", i, j, k];
            }
        }
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *filePath = [documentsDir stringByAppendingPathComponent:@"num3bit"];
    NSError *err = [[NSError alloc] init];
    [str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&err];
    if (err.code == 0) {
        NSLog(@"保存成功");
    }else {
        NSLog(@"err: 保存失败 - %@", err);
    }
}

- (void)createNum2Bit
{
    NSString *str = @"";
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            str = [str stringByAppendingFormat:@"%d%d\n", i, j];
        }
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *filePath = [documentsDir stringByAppendingPathComponent:@"num2bit"];
    NSError *err = [[NSError alloc] init];
    [str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&err];
    if (err.code == 0) {
        NSLog(@"保存成功");
    }else {
        NSLog(@"err: 保存失败 - %@", err);
    }
}

@end
